Source: astropy-healpix
Maintainer: Debian Astronomy Maintainers <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Leo Singer <leo.singer@ligo.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all-dev,
               python3-astropy,
               python3-extension-helpers,
               python3-healpy,
               python3-hypothesis,
               python3-numpy,
               python3-pytest,
               python3-setuptools,
               python3-setuptools-scm
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/astropy-healpix
Vcs-Git: https://salsa.debian.org/debian-astro-team/astropy-healpix.git
Homepage: https://astropy-healpix.readthedocs.io

Package: python3-astropy-healpix
Architecture: any
Depends: ${misc:Depends}, ${python3:Depends}, ${shlibs:Depends}
Description: HEALPix representation of spherical data - Python 3
 HEALPix is an acronym for Hierarchical Equal Area isoLatitude Pixelization
 of a sphere. As suggested in the name, this pixelization produces a
 subdivision of a spherical surface in which each pixel covers the same
 surface area as every other pixel. It is commonly used to store all-sky
 astronomical images, most famously maps of the cosmic microwave background.
 .
 This is a BSD-licensed HEALPix package developed by the Astropy project
 and based on C code written by Dustin Lang in astrometry.net.
 .
 This package provides modules for Python 3.
